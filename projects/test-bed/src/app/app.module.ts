import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {DhmStarRatingComponent} from '../../../dhm-star-rating/src/lib/dhm-star-rating.component';
import {FormsModule} from '@angular/forms';

@NgModule( {
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DhmStarRatingComponent,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
} )
export class AppModule {
}
