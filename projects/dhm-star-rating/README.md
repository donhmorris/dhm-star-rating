# dhm-star-rating

Angular widget for providing a rating using stars.

## Why another star rating widget for Angular?

Because the ones I found inevitably had a flaw that really bugged me.

## Features

- Complete control of colors: selected, non-selected, highlight.
- No external dependencies other than Angular (v15).
- High quality SVG's for stars
- Efficient. Uses one SVG definition for each star.
- Is created as an inline element (span) to provide the greatest flexibility.
- Support for half stars.
- Readonly mode.
- Unset mode. (Rating is zero.)
- Sizing control.

[Documentation](https://bitbucket.org/donhmorris/dhm-star-rating/wiki/Home)

## History

- v2 updated as standalone component and angular 15
- v1.0 original version
