import {TestBed} from '@angular/core/testing';

import {DhmStarRatingService} from './dhm-star-rating.service';

describe( 'DhmStarRatingService', () => {
  let service: DhmStarRatingService;

  beforeEach( () => {
    TestBed.configureTestingModule( {} );
    service = TestBed.inject( DhmStarRatingService );
  } );

  it( 'should be created', () => {
    expect( service ).toBeTruthy();
  } );
} );
