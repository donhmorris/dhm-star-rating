import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CommonModule} from '@angular/common';
import {svgs} from './svgs';

@Component( {
  selector: 'dhm-star-rating',
  template: `
    <span>
      <svg (click)="doSetValue(i + 1)" (mouseenter)="hoverItem = i + 1"
           (mouseleave)="hoverItem = 0" *ngFor="let item of stars; index as i"
           [style.cursor]="readOnly ? 'default':'pointer'"
           [style.fill]="getStarColor(i+1)" [style.height]="size" [style.width]="size">
        <use *ngIf="!useHalfStar(i + 1)" href="#dhm-str-star"></use>
        <use *ngIf="useHalfStar(i + 1)" xlink:href="#dhm-str-star-half"></use>
      </svg>
    </span>
  `,
  styles: [`span {
    display: flex;
    justify-content: flex-start;
  }`,
    `svg {
      aspect-ratio: 1/ 1;
    }`
  ],
  standalone: true,
  imports: [CommonModule]
} )
export class DhmStarRatingComponent implements OnInit {
  @Input() useHalfStars = true;
  @Input() readOnly = false;
  @Input() hoverColor = 'darkgoldenrod';
  @Input() size = '24px';
  @Input() selectedColor = 'goldenrod';
  @Input() unselectedColor = 'lightgray';
  @Input() starCount = 5;

  stars: number[] = [];
  hoverItem = 0;
  @Output() valueChange = new EventEmitter();


  protected mValue = 0;

  get value(): number {
    return this.mValue;
  }


  @Input()
  set value( theValue: number ) {
    this.mValue = theValue;
    this.valueChange.emit( theValue );
  }

  ngOnInit(): void {
    this.initStarTemplate();
    for ( let i = 0; i < this.starCount; i++ ) {
      this.stars.push( 0 );
    }
  }

  getStarColor( theIndex: number ): string {
    return this.hoverItem === theIndex && !this.readOnly ? this.hoverColor :
      (theIndex <= this.value || this.useHalfStar( theIndex ) ? this.selectedColor : this.unselectedColor);
  }

  doSetValue( theValue: number ): void {
    if ( !this.readOnly ) {
      this.value = theValue;
    }
  }

  useHalfStar( theIndex: number ): boolean {
    if ( !this.useHalfStars ) {
      return false;
    }
    const fract = this.value % 1;
    const inHalfRange = fract >= 0.33 && fract <= 0.99999999;
    const intPart = Math.trunc( this.value );
    return inHalfRange && theIndex === intPart + 1;
  }

  protected startemplateIsLoaded(): boolean {
    return !!document.getElementById( 'dhm-str-svg-template' );
  }

  protected initStarTemplate() {
    if ( !this.startemplateIsLoaded() ) {
      document.body.insertAdjacentHTML( 'afterbegin', svgs );
    }
  }

}






























