import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DhmStarRatingComponent} from './dhm-star-rating.component';

describe( 'DhmStarRatingComponent', () => {
  let component: DhmStarRatingComponent;
  let fixture: ComponentFixture<DhmStarRatingComponent>;

  beforeEach( async () => {
    await TestBed.configureTestingModule( {
      declarations: [DhmStarRatingComponent]
    } )
      .compileComponents();

    fixture = TestBed.createComponent( DhmStarRatingComponent );
    component = fixture.componentInstance;
    fixture.detectChanges();
  } );

  it( 'should create', () => {
    expect( component ).toBeTruthy();
  } );
} );
