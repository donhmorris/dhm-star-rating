/*
 * Public API Surface of dhm-star-rating
 */

export * from './lib/dhm-star-rating.service';
export * from './lib/dhm-star-rating.component';
